import React, { Component } from "react";
import Search from "./Search";
import Calculate from "./Calculate";
import Portfolio from "./Portfolio";
import axios from "axios";

class PortfolioContainer extends Component {
    constructor(props) {
        super(props);

        this.state = {
            portfolio: [],
            search_results: [],
            active_currency: null,
            amount: '',
        };

        this.handleChange = this.handleChange.bind(this)
        this.handleSelect = this.handleSelect.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleAmount = this.handleAmount.bind(this)
    }

    handleChange(e) {
        const searchTerm = e.target.value;

        this.setState({
            name: searchTerm
        }, () => {
            axios.post('http://localhost:3000/search', {
                search: searchTerm
            })
                .then((response) => {
                    this.setState({
                        search_results: response.data.currencies
                    });
                })
                .catch((error) => {
                    console.error('Hiba:', error);
                });
        });
    }

    handleSelect(e){
        e.preventDefault()
        const id = e.target.getAttribute('data-id')
        const activeCurrency = this.state.search_results.filter( item => item.id === parseInt(id))
        this.setState({
            active_currency: activeCurrency[0],
            search_results: []
        })
    }

    handleSubmit = (e) => {
        e.preventDefault()

        let currency = this.state.active_currency
        let amount = e.target.amount.value

        axios.post('http://localhost:3000/calculate', {
            id: currency.id,
            amount: amount
        })
            .then( (data) => {
                this.setState({
                    amount: '',
                    active_currency: null,
                    portfolio: [...this.state.portfolio, data.data]
                })
                console.log(data)
            })
            .catch( (err) => console.log(err))
    }

    handleAmount(e){
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    render() {
        const searchOrCalculate = this.state.active_currency ?
            <Calculate
                handleChange={this.handleAmount}
                handleSubmit={this.handleSubmit}
                active_currency={this.state.active_currency}
                amount={this.state.amount}
            /> :
            <Search
                handleSelect={this.handleSelect}
                handleChange={this.handleChange}
                name={this.state.name}
                searchResults={this.state.search_results}
            />;

        return(
            <div className="grid">
                <div className="left">
                    {searchOrCalculate}
                </div>
                <div className="right">
                    <Portfolio portfolio={this.state.portfolio}/>
                </div>
            </div>
        );
    }
}

export default PortfolioContainer;