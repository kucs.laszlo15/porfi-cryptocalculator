import React, {Component} from "react";

class Calculate extends Component {
    constructor(props) {
        super(props);
        this.state = {
            amount: ''
        };
    }

    // Metódus az input mező értékének frissítésére
    handleChange = (event) => {
        this.setState({
            amount: event.target.value
        });
    }

    render() {
        return (
            <div>
                <h1>How much {this.props.active_currency.name} do you own?</h1>
                <form onSubmit={this.props.handleSubmit}>
                    <div className="formgroup">
                        <label>Enter amount of: </label><br/>
                        <input
                            onChange={this.handleChange} // Az onChange esemény hívja meg a handleChange metódust
                            autoComplete="off"
                            type="text"
                            name="amount"
                            placeholder="How much do you own?"
                            value={this.state.amount} // A state-ből származó érték
                            className="field"
                        />
                    </div>
                    <div className="form-group">
                        <input type="submit" className="calculate-btn" value="Calculate my total"/>
                    </div>
                </form>
            </div>
        )
    }
}

export default Calculate;
