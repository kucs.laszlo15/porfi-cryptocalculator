"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["index"],{

/***/ "./app/javascript/components/App.js":
/*!******************************************!*\
  !*** ./app/javascript/components/App.js ***!
  \******************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _PortfolioContainer__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./PortfolioContainer */ "./app/javascript/components/PortfolioContainer.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! axios */ "./node_modules/axios/lib/axios.js");



const csrfToken = document.querySelector('[name="csrf-token"]').content;
axios__WEBPACK_IMPORTED_MODULE_2__["default"].defaults.headers.common['X-CSRF-TOKEN'] = csrfToken;
class App extends react__WEBPACK_IMPORTED_MODULE_0__.Component {
  render() {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default().createElement(_PortfolioContainer__WEBPACK_IMPORTED_MODULE_1__["default"], null);
  }
}
/* harmony default export */ __webpack_exports__["default"] = (App);

/***/ }),

/***/ "./app/javascript/components/Calculate.js":
/*!************************************************!*\
  !*** ./app/javascript/components/Calculate.js ***!
  \************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);

class Calculate extends react__WEBPACK_IMPORTED_MODULE_0__.Component {
  constructor(props) {
    super(props);
    this.state = {
      amount: ''
    };
  }

  // Metódus az input mező értékének frissítésére
  handleChange = event => {
    this.setState({
      amount: event.target.value
    });
  };
  render() {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default().createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default().createElement("h1", null, "How much ", this.props.active_currency.name, " do you own?"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default().createElement("form", {
      onSubmit: this.props.handleSubmit
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default().createElement("div", {
      className: "formgroup"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default().createElement("label", null, "Enter amount of: "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default().createElement("br", null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default().createElement("input", {
      onChange: this.handleChange // Az onChange esemény hívja meg a handleChange metódust
      ,
      autoComplete: "off",
      type: "text",
      name: "amount",
      placeholder: "How much do you own?",
      value: this.state.amount // A state-ből származó érték
      ,
      className: "field"
    })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default().createElement("div", {
      className: "form-group"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default().createElement("input", {
      type: "submit",
      className: "calculate-btn",
      value: "Calculate my total"
    }))));
  }
}
/* harmony default export */ __webpack_exports__["default"] = (Calculate);

/***/ }),

/***/ "./app/javascript/components/Portfolio.js":
/*!************************************************!*\
  !*** ./app/javascript/components/Portfolio.js ***!
  \************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _PortfolioItem__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./PortfolioItem */ "./app/javascript/components/PortfolioItem.js");


class Portfolio extends react__WEBPACK_IMPORTED_MODULE_0__.Component {
  constructor(props) {
    super(props);
  }
  render() {
    const portfolioItems = this.props.portfolio.map((item, index) => /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default().createElement(_PortfolioItem__WEBPACK_IMPORTED_MODULE_1__["default"], {
      key: index,
      item: item
    }));
    const total = this.props.portfolio.reduce((total, curr) => total + curr.value, 0);
    const formatted_total = total.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default().createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default().createElement("div", {
      className: "portfolio-value"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default().createElement("div", {
      className: "portfolio-value--header"
    }, "Your Total Portfolio Value Is:"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default().createElement("div", {
      className: "portfolio-value--content"
    }, "$", formatted_total)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default().createElement("div", {
      className: "portfolio-items"
    }, portfolioItems));
  }
}
/* harmony default export */ __webpack_exports__["default"] = (Portfolio);

/***/ }),

/***/ "./app/javascript/components/PortfolioContainer.js":
/*!*********************************************************!*\
  !*** ./app/javascript/components/PortfolioContainer.js ***!
  \*********************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _Search__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Search */ "./app/javascript/components/Search.js");
/* harmony import */ var _Calculate__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Calculate */ "./app/javascript/components/Calculate.js");
/* harmony import */ var _Portfolio__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./Portfolio */ "./app/javascript/components/Portfolio.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! axios */ "./node_modules/axios/lib/axios.js");





class PortfolioContainer extends react__WEBPACK_IMPORTED_MODULE_0__.Component {
  constructor(props) {
    super(props);
    this.state = {
      portfolio: [],
      search_results: [],
      active_currency: null,
      amount: ''
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSelect = this.handleSelect.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleAmount = this.handleAmount.bind(this);
  }
  handleChange(e) {
    const searchTerm = e.target.value;
    this.setState({
      name: searchTerm
    }, () => {
      axios__WEBPACK_IMPORTED_MODULE_4__["default"].post('http://localhost:3000/search', {
        search: searchTerm
      }).then(response => {
        this.setState({
          search_results: response.data.currencies
        });
      }).catch(error => {
        console.error('Hiba:', error);
      });
    });
  }
  handleSelect(e) {
    e.preventDefault();
    const id = e.target.getAttribute('data-id');
    const activeCurrency = this.state.search_results.filter(item => item.id === parseInt(id));
    this.setState({
      active_currency: activeCurrency[0],
      search_results: []
    });
  }
  handleSubmit = e => {
    e.preventDefault();
    let currency = this.state.active_currency;
    let amount = e.target.amount.value;
    axios__WEBPACK_IMPORTED_MODULE_4__["default"].post('http://localhost:3000/calculate', {
      id: currency.id,
      amount: amount
    }).then(data => {
      this.setState({
        amount: '',
        active_currency: null,
        portfolio: [...this.state.portfolio, data.data]
      });
      console.log(data);
    }).catch(err => console.log(err));
  };
  handleAmount(e) {
    this.setState({
      [e.target.name]: e.target.value
    });
  }
  render() {
    const searchOrCalculate = this.state.active_currency ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default().createElement(_Calculate__WEBPACK_IMPORTED_MODULE_2__["default"], {
      handleChange: this.handleAmount,
      handleSubmit: this.handleSubmit,
      active_currency: this.state.active_currency,
      amount: this.state.amount
    }) : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default().createElement(_Search__WEBPACK_IMPORTED_MODULE_1__["default"], {
      handleSelect: this.handleSelect,
      handleChange: this.handleChange,
      name: this.state.name,
      searchResults: this.state.search_results
    });
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default().createElement("div", {
      className: "grid"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default().createElement("div", {
      className: "left"
    }, searchOrCalculate), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default().createElement("div", {
      className: "right"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default().createElement(_Portfolio__WEBPACK_IMPORTED_MODULE_3__["default"], {
      portfolio: this.state.portfolio
    })));
  }
}
/* harmony default export */ __webpack_exports__["default"] = (PortfolioContainer);

/***/ }),

/***/ "./app/javascript/components/PortfolioItem.js":
/*!****************************************************!*\
  !*** ./app/javascript/components/PortfolioItem.js ***!
  \****************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);

const PortfolioItem = props => {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default().createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default().createElement("div", {
    className: "row"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default().createElement("div", {
    className: "col"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default().createElement("div", {
    className: "header"
  }, "Currency:"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default().createElement("div", {
    className: "text"
  }, props.item.currency.name)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default().createElement("div", {
    className: "col"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default().createElement("div", {
    className: "header"
  }, "Current Price:"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default().createElement("div", {
    className: "text"
  }, "$", props.item.current_price)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default().createElement("div", {
    className: "col"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default().createElement("div", {
    className: "header"
  }, "Amount In Your Portfolio:"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default().createElement("div", {
    className: "text"
  }, props.item.amount)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default().createElement("div", {
    className: "col"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default().createElement("div", {
    className: "header"
  }, "Current Value:"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default().createElement("div", {
    className: "text"
  }, "$", props.item.value))));
};
/* harmony default export */ __webpack_exports__["default"] = (PortfolioItem);

/***/ }),

/***/ "./app/javascript/components/Search.js":
/*!*********************************************!*\
  !*** ./app/javascript/components/Search.js ***!
  \*********************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);

class Search extends react__WEBPACK_IMPORTED_MODULE_0__.Component {
  constructor(props) {
    super(props);
  }
  render() {
    const searchResults = this.props.searchResults.map(curr => /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default().createElement("li", {
      key: curr.id,
      "data-id": curr.id,
      onClick: this.props.handleSelect,
      className: "currency-list-item"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default().createElement("a", {
      href: "#",
      className: "currency"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default().createElement("span", null, curr.name), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default().createElement("span", null, curr.currency_symbol))));
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default().createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default().createElement("h1", null, "Porfi - Crypto Portfolio Calculator"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default().createElement("form", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default().createElement("div", {
      className: "form-group"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default().createElement("label", null, "Search for a Currency:"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default().createElement("br", null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default().createElement("input", {
      onChange: this.props.handleChange,
      autoComplete: "off",
      type: "text",
      name: "name",
      placeholder: "Ex: Bitcoin, Litecoin, Ethereum...",
      value: this.props.name,
      className: "field"
    })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default().createElement("div", {
      className: "currency-list"
    }, searchResults)));
  }
}
/* harmony default export */ __webpack_exports__["default"] = (Search);

/***/ }),

/***/ "./app/javascript/packs/index.js":
/*!***************************************!*\
  !*** ./app/javascript/packs/index.js ***!
  \***************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-dom */ "./node_modules/react-dom/index.js");
/* harmony import */ var _components_App__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../components/App */ "./app/javascript/components/App.js");



document.addEventListener('DOMContentLoaded', () => {
  react_dom__WEBPACK_IMPORTED_MODULE_1__.render( /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default().createElement(_components_App__WEBPACK_IMPORTED_MODULE_2__["default"], null), document.body.appendChild(document.createElement('div')));
});

/***/ })

},
/******/ function(__webpack_require__) { // webpackRuntimeModules
/******/ var __webpack_exec__ = function(moduleId) { return __webpack_require__(__webpack_require__.s = moduleId); }
/******/ __webpack_require__.O(0, ["vendors-node_modules_react-dom_index_js-node_modules_axios_lib_axios_js"], function() { return __webpack_exec__("./app/javascript/packs/index.js"); });
/******/ var __webpack_exports__ = __webpack_require__.O();
/******/ }
]);
//# sourceMappingURL=index.js.map