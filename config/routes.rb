Rails.application.routes.draw do
  get 'currencies/index'
  get 'currencies/search'
  get 'currencies/calculate'
  root 'currencies#index'
  post 'search', to: 'currencies#search'
  post 'calculate', to: 'currencies#calculate'
end